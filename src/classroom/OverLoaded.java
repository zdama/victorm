package classroom;

public class OverLoaded {

        void show (boolean b) { System.out.println(b); }
        void show ( int i) {
            System.out.println(i);
        }
        void show ( float f) {
            System.out.println(f);
        }
        void show ( double d){
            System.out.println(d);
        }
        void show ( int mm, boolean dd){
            System.out.println(mm + " " + dd);
    }
}

class OverLoadedTest {
        public static void main (String [] arhs) {

            OverLoaded tip1 = new OverLoaded();
            boolean b1 = true;
            tip1.show(b1);
            int i1 = 39;
            tip1.show(i1);
            float f1=6.5f;
            tip1.show(f1);
            double d1=235954757;
            tip1.show(d1);
            double d2=343435;
            tip1.show(d2);
            int mm = 50;
            boolean dd = false;
            tip1.show(mm,dd);
            }
}


