package classroom;

public class Student {
int studentID;
String name;
String surname;
byte course;
double mathAverageGrade;
double physicAverageGrade;
double chemistryAverageGrade;

}

class studentResult {

    double anualAverageGrade(Student st) {
        double courseAverageGrade = (st.chemistryAverageGrade + st.mathAverageGrade + st.physicAverageGrade)/3;

        System.out.println("Stundetul " + st.name + " " + st.surname + ", cu ID " + st.studentID +  " in cursul "
                + st.course + ", are media anuala de " + courseAverageGrade);
        return courseAverageGrade;
    }
    public static void main (String [] args) {
        Student st1 = new Student();
        st1.studentID = 49485;
        st1.name = "Victor";
        st1.surname = "Manolache";
        st1.course = 3;
        st1.mathAverageGrade = 8.4;
        st1.physicAverageGrade = 9.5;
        st1.chemistryAverageGrade = 6.7;

        Student st2 = new Student();
        st2.studentID = 47894;
        st2.name = "Ion";
        st2.surname = "Grecu";
        st2.course = 4;
        st2.mathAverageGrade = 6.9;
        st2.physicAverageGrade = 7.3;
        st2.chemistryAverageGrade = 8.9;

        Student st3 = new Student();
        st3.studentID = 32678;
        st3.name = "Mihaela";
        st3.surname = "Cheplat";
        st3.course = 2;
        st3.mathAverageGrade = 8.4;
        st3.physicAverageGrade = 6.5;
        st3.chemistryAverageGrade = 8.7;

        studentResult ViewResult = new studentResult();
        ViewResult.anualAverageGrade(st1);
        ViewResult.anualAverageGrade(st2);
        ViewResult.anualAverageGrade(st3);
    }


}