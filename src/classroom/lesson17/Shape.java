package classroom.lesson17;

public abstract class Shape {
    private String color;

    public abstract void draw ();
    public String getColor (String color) {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        System.out.println(" " + color);
    }
}
