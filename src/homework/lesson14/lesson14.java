package homework.lesson14;

public class lesson14 {

    public lesson14 (int a) {
     this.a = a;
    }

    lesson14(int a, int b) {
     this(a);
     this.b = b;
    }

    lesson14(int a, int b, int c) {
     this(a, b);
     this.c = c;
    }

    int a;
    int b;
    int c;

}

class lesson14Test {

    public static void main (String[] args) {

        lesson14 metoda1 = new lesson14(5);
        System.out.println (metoda1.a);
        lesson14 metoda2 = new lesson14(5, 3);
        System.out.println (metoda1.a + " " + metoda2.b);
        lesson14 metoda3 = new lesson14(4,5,4);
        System.out.println (metoda3.a + " " + metoda3.b + " " + metoda3.c);





    }
}
/*

Homework:
 1. One class with two or three methods. In the first method call the second and third method with "this" keyword.
 2. One class with 3 oveloaded methods.
 3. One class with three overloaded constructors.Use "this" to assign the passed parameters to class properties.
 4. One class with three overloaded constructors using "this()".Use "this" to assign the passed parameters to class properties.
 5. One class with a method that has a parameter.When calling the method, it should be posible to pass "this" as parameter.
 6. One class with a method or two that return a type of the current class. Hint: using "this".
 */