package homework.lesson15;
import java.util.Arrays;
import java.util.stream.IntStream;
public class ArrayEx {

    public static void main(String[] args) {
        int[] intArr1 = {1, 3, 4, 5};
        int sum = IntStream.of(intArr1).sum();
        int suma = 0;
        System.out.println(sum); /* ex1-1 */

        for (int i = 0; i < intArr1.length; i++) {
            suma = suma + intArr1[i]; /* ex1-2*/
        }
        System.out.println(suma);

        System.out.println("Numere impare: ");
        for (int i = 0; i < intArr1.length; i++) {
            if (intArr1[i] % 2 != 0) {

                System.out.println(intArr1[i]); /* ex2 */

                int[] arr2 = new int[intArr1[i]]; /* ex2 v2 */
                System.out.println("Solutia 2: " + arr2.length); /* ex2 v2 */
            }

        }
        System.out.println("Numere pare:");

        for (int i = 0; i < intArr1.length; i++) {
            if (intArr1[i] % 2 == 0) {
                System.out.println(intArr1[i]); /* ex3 */

                int[] arr2 = new int[intArr1[i]]; /* ex3 v2 */
                System.out.println("Solutia 2: " + arr2.length); /* ex3 v2 */
            }

        }

        int[][] multiples = {{1,3,5},
                            {2,4,6}};

        System.out.println(Arrays.deepToString(multiples));



    }
            }





/*
        1. Creati o metoda care primeste un array de int-uri si returneaza suma tuturor numerelor
        2. Creati o metoda care primeste un array si returneaza toate numerele pare
        3. Creati o metoda care primeste un array si returneaza toate numerele impare
        4. Creati o metoda care primeste un array bidimensional.
           Traversati acest array si printati elemetelem sub forma de matrice
 */

/* initializare array
int Arr [] = {1,2,3};
int Arr [] = new int [3];
int arr [] = new int [] {4,5,6};
int suma = sumaARRAY(new int [] {4,5,6};
 */