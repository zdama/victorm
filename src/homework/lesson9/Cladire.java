package homework.lesson9;

public class Cladire {

    String adresa;
    byte etaje;
    int ferestre;
    int locatari;
    short apartamente;
    float rezistentaSeismica;
    boolean exploatare;


    public void tipCladire () {

        System.out.println("Aceasta cladire este rezidentiala");
    }

    public float ValoareRezistentaSeismica (float rezistentaSeismica) {

        return rezistentaSeismica;

}
 public void statut (int locatari, boolean exploatare) {

        System.out.println("Acesta cladire are " + locatari + " de locatari si este data in exploatare: " + exploatare );
    }

    public String locatia (String adresa) {

        return adresa;
  
    }

}



/*2. In clasa de mai sus construiti:
        a. o metoda care nu returneaza nimic si nu primeste nici un parametru de intrare
        b. o metoda care returneaza un float si nu primeste nici un paramentru de intrare
        c. o metoda care primeste 2 parametri de intrare de tip diferit si nu returneaza nimic
        d. o metoda care primeste 1 parametru de intrare si returneaza un string
        e. creati o alta clasa in care sa aveti metoda main, in ea construiti obiecte de la clasa de mai sus si apelati toate metodele
        3. Creati o alta clasa, in ea sa aveti o metoda care gaseste toate numerele pare pina la un anumit nr intreg folosind ciclul for.
        a. creati o alta clasa in care sa aveti metoda main, in ea construiti un obiect, apelati metoda pentru a demostra ca ea lucreaza
*/