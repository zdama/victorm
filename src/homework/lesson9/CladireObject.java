package homework.lesson9;

public class CladireObject {
    public static void main (String [] args) {


        Cladire Cladirea1 = new Cladire();
        Cladirea1.rezistentaSeismica = 7.6f;
        Cladirea1.tipCladire ();
        Cladirea1.etaje =3;
        Cladirea1.ValoareRezistentaSeismica(0f);
        Cladirea1.statut(65, true);
        String AdresaCladire1  = Cladirea1.locatia("Stefan Cel Mare 180");

        System.out.println("Cladirea are valoarea seismica de "+Cladirea1.rezistentaSeismica);
        System.out.println("Cladirea are "+Cladirea1.etaje + " etaje");
        System.out.println("Cladirea este situata pe str " + AdresaCladire1);

    }
}