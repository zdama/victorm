package maculator;

public class interation1 {
    public static void main(String[] args) {
        for (int iteration = 0; iteration < 1; iteration++) {
            for (int innerIteration = 0; innerIteration <= 10; innerIteration++) {
                if (innerIteration % 2 > 0) {
                    continue;
                }
                System.out.println(innerIteration);
            }
        }
    }
}